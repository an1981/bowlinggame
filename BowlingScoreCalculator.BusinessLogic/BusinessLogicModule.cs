﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace BowlingScoreCalculator.BusinessLogic
{
    public static class BusinessLogicModule
    {
        public static IServiceCollection AddBusinessLogicModule(this IServiceCollection services)
        {
            services.AddTransient<IScoreCalculator, ScoreCalculator>();
            return services;
        }
    }
}
