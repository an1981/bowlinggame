﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BowlingScoreCalculator.BusinessLogic.Exceptions
{
    public class InvalidDownedPinException : BowlingScoreCalculatorException
    {
        public InvalidDownedPinException(string message) : base(message)
        {
            Code = ErrorCode.INVALID_DOWNED_PIN;
        }
    }
}
