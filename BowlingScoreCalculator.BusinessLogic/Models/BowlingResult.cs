﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BowlingScoreCalculator.BusinessLogic.Models
{
    public class BowlingResult
    {
        public IEnumerable<string> FrameProgressScores { get; set; }
        public bool GameCompleted { get; set; }

    }
    
}
