# README #

This README would normally document whatever steps are necessary to get your application up and running.
######## External Libs ##################
1) Microsoft.AspNetCore.Mvc.Testing -- Support for functional tests for MVC applications

####### Exception Handling #################
Web API errors are handled via global filters. 
IF the ids passed is > 0 or > 10  or the number of inputs exceed 21 the filters should be catching the exception 
 
########### Logging ###################### 
Standard asp .net core logging in place. 

#### Tests #################################
The controller tests checks for the status code
The businesslogic test checks for various params and the respective outputs


######### Deployment ######################
The code is deployed on  https://bowlingscorecalculatorapi.azurewebsites.net/swagger/index.html 

