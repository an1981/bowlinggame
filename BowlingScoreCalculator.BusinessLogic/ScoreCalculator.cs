﻿using BowlingScoreCalculator.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using BowlingScoreCalculator.BusinessLogic.Exceptions;

namespace BowlingScoreCalculator.BusinessLogic
{
    public class ScoreCalculator : IScoreCalculator
    {
        private int[] rolls;
        private int[] frame;
        int currentRoll = 0;
        int totalFrames = 10;
        int maxPoints = 10;
        private string strikeScore = "*";
        public ScoreCalculator()
        {
            rolls = new int[21];
            for (int i = 0; i < 21; i++)
            {
                rolls[i] = -1;
            }
            frame = new int[totalFrames];

        }
        public BowlingResult GetScore(int[] pinsDowned)
        {
            ValidateRequest(pinsDowned);
            //validation of integers .. 

            // Could have separate exceptions to make it more meaningful in terms of error code.
            // Just keeping it simple for now


            Roll(pinsDowned);

           var scoreList = CalculateScore();
            return new BowlingResult()
            {
                FrameProgressScores = scoreList,
                GameCompleted = IsGameCompleted(scoreList)
            };
        }
        private void ValidateRequest(int[] pinsDowned)
        {
            if (pinsDowned.Any(p => p < 0))
            {
                throw new InvalidDownedPinException("entered pin has value less than 0");
            }
            if (pinsDowned.Any(p => p > 10))
            {
                throw new InvalidDownedPinException("entered pin has value more than 10");
            }

            if (pinsDowned.Length > 21)
            {
                throw new InvalidDownedPinException("No of pins downed is more than the number of frames/chances");
            }

            if (pinsDowned.Any(s => s < 10))
            {
                int countOf10 = pinsDowned.Where(s => s == 10).Count();
                if ((pinsDowned.Length - countOf10) + (countOf10 * 2) > 20)
                {
                    throw new InvalidDownedPinException("Number of pins downed is more than the chances you have ");
                }
            }
            else
            {
                if (pinsDowned.Length > 12)
                {
                    throw new InvalidDownedPinException("Number of pins downed as 10s cannot exceed 12");
                }
            }
        }
        private bool IsGameCompleted(List<string> scoreList)
        {
            if(scoreList.Count != totalFrames)
            {
                return false;
            }
            if (scoreList.Contains(strikeScore))
            {
                return false;
            }
            return true;
                 
                
        }

        private bool isSpare(int frameIndex)
        {
            return rolls[frameIndex] + rolls[frameIndex + 1] == maxPoints;
        }

        private bool isStrike(int frameIndex)
        {
            return rolls[frameIndex] == maxPoints;
        }

         

        private void Roll(int[] pins)
        {
            for (int i = 0; i < pins.Length; i++)
            {
                rolls[currentRoll++] = pins[i];
            }
        }
        private List<string> CalculateScore()
        {
            int[] strikeFrame = new int[totalFrames];

            List<string> outData = new List<string>();
            int score = 0;
            int frameIndex = 0;
            for (int frame = 0; frame < totalFrames; frame++)
            {
                bool isPartialFrame = false;
                bool isStrikeFrame = false;
                if (rolls[frameIndex] == -1)
                {
                    break;
                }
                if (isSpare(frameIndex))
                {

                    score += maxPoints + rolls[frameIndex + 2];
                    frameIndex += 2;
                }
                else if (isStrike(frameIndex))
                {
                    isStrikeFrame = true;
                    score += maxPoints + rolls[frameIndex + 1] + rolls[frameIndex + 2];

                    strikeFrame[frame] = score;

                    frameIndex++;
                }
                else
                {
                    if(rolls[frameIndex]+ rolls[frameIndex+1] > maxPoints)
                    {
                        throw new InvalidDownedPinException($"Sum of pins in a frame cannot be greater than {maxPoints}");
                    }
                    if(rolls[frameIndex+1] == -1)
                    {
                        isPartialFrame = true;
                    }
                    score += rolls[frameIndex] + rolls[frameIndex + 1];
                    frameIndex += 2;
                }
                if ((isStrikeFrame) || (isPartialFrame))
                {
                    outData.Add(strikeScore);
                }
                else
                {
                    outData.Add(score.ToString());
                }
            }
            var data = GetActualScoreWithStrike(outData, strikeFrame);
            return data;
        }
        private int GetMaxLoopCount(List<string> score, int[] strikeFrame)
        {
            if(score.Any(s=>s!= strikeScore))
            {
                return totalFrames;
            }
            
            if(strikeFrame[totalFrames-1] == 300)
            {
                return totalFrames;
            }
                return totalFrames-1;
        }
        private List<string> GetActualScoreWithStrike(List<string> score, int[] strikeFrame)
        {
            int maxLoopCount = GetMaxLoopCount(score, strikeFrame);
            for (int i = 0; i < score.Count; i++)
            {
                if (score[i] == strikeScore)
                {
                    if (i == maxLoopCount - 1)
                    {
                        score[i] = strikeFrame[i].ToString();
                        if (score[i - 1] == strikeScore)
                        {
                            score[i - 1] = strikeFrame[i - 1].ToString();
                        }
                        if (score[i - 2] == strikeScore)
                        {
                            score[i - 2] = strikeFrame[i - 2].ToString();
                        }

                    }
                    else
                    {
                        if ((i + 1) >= score.Count)
                        {
                            break;
                        }

                        if (score[i + 1] != strikeScore)
                        {
                            score[i] = strikeFrame[i].ToString();
                        }
                        if (i >= 1)
                        {
                            if (score[i - 1] == strikeScore)
                            {
                                score[i - 1] = strikeFrame[i - 1].ToString();
                            }
                        }

                    }
                }
            }
            return score;
        }
    }
}
