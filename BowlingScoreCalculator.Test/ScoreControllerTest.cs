﻿using BowlingScoreCalculator.API.Requests;
using BowlingScoreCalculator.Test.Fixtures;
using BowlingScoreCalculator.Test.Utils;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BowlingScoreCalculator.Test
{
    public class ScoreControllerTest : ComponentTest
    {
        string scoreUrl = "api/score";
        public ScoreControllerTest(ApiWebApplicationFactory factory)
                  : base(factory)
        {
            


        }
        [Fact]
        public async Task Get_Bad_Request_for_Pin_less_than_0()
        {
            int[] pinsDowned = new int[] { -1, 2 };
            PinsDownedRequest request = new PinsDownedRequest()
            {
                pinsDowned = pinsDowned
            };

            var response = await _client.PostAsJsonAsync<PinsDownedRequest>($"{scoreUrl}", request);
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);

        }
        [Fact]
        public async Task Get_Bad_Request_for_Pin_Greater_than_10()
        {
            int[] pinsDowned = new int[] { 11,2 };
            PinsDownedRequest request = new PinsDownedRequest()
            {
                pinsDowned = pinsDowned
            };
            var response = await _client.PostAsJsonAsync<PinsDownedRequest>($"{scoreUrl}", request);
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);

        }
        [Fact]
        public async Task Get_Bad_Request_for_Number_Of_Pin_Exceeding_limit()
        {
            int[] pinsDowned = new int[] { 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, 2 };
            PinsDownedRequest request = new PinsDownedRequest()
            {
                pinsDowned = pinsDowned
            };
            var response = await _client.PostAsJsonAsync<PinsDownedRequest>($"{scoreUrl}", request);
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);

        }
        [Fact]
        public async Task Get_Ok_For_Valid_Request()
        {
            int[] pinsDowned = new int[] { 1, 1, 1, 1 };
            PinsDownedRequest request = new PinsDownedRequest()
            {
                pinsDowned = pinsDowned
            };
            var response = await _client.PostAsJsonAsync<PinsDownedRequest>($"{scoreUrl}", request);
            response.StatusCode.Should().Be(HttpStatusCode.OK);

        }
    }
}
