﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace BowlingScoreCalculator.Test.Utils
{
    public static class HttpExtensions
    {
        public static async Task<T> GetAndDeserialize<T>(this HttpResponseMessage message)
        {
            message.EnsureSuccessStatusCode();
            return JsonConvert.DeserializeObject<T>(await message.Content.ReadAsStringAsync());

        }
        public static Task<HttpResponseMessage> PostAsJsonAsync<T>(
           this HttpClient httpClient, string url, T data)
        {
            string dataAsString = JsonConvert.SerializeObject(data);
            var content = new StringContent(dataAsString);
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return httpClient.PostAsync(url, content);
        }
    }
}
