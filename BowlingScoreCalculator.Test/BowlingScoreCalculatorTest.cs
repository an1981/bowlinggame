﻿using BowlingScoreCalculator.BusinessLogic;
using BowlingScoreCalculator.BusinessLogic.Exceptions;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace BowlingScoreCalculator.Test
{
    public class BowlingScoreCalculatorTest
    {
        private IScoreCalculator _scoreCalculator { get; set; }
        public BowlingScoreCalculatorTest()
        {
            _scoreCalculator = new ScoreCalculator();
        }
        [Fact]
        public void CalculateGutterBallGame()
        {
            int[] pinsDowned = new int[20];
            var result = _scoreCalculator.GetScore(pinsDowned);
            result.GameCompleted.Should().BeTrue();
            result.FrameProgressScores.ToList()[0].Should().Be("0");
        }

        [Fact]
        public void CalculatePerfectBallGame()
        {
            int[] pinsDowned = new int[12];
            for (int i = 0; i < pinsDowned.Length; i++)
            {
                pinsDowned[i] = 10;
            }
            var result = _scoreCalculator.GetScore(pinsDowned);
            result.GameCompleted.Should().BeTrue();
            result.FrameProgressScores.ToList()[9].Should().Be("300");
            result.FrameProgressScores.ToList()[8].Should().Be("270");
            result.FrameProgressScores.ToList()[7].Should().Be("240");
            result.FrameProgressScores.ToList()[6].Should().Be("210");
            result.FrameProgressScores.ToList()[5].Should().Be("180");
            result.FrameProgressScores.ToList()[4].Should().Be("150");
            result.FrameProgressScores.ToList()[3].Should().Be("120");
            result.FrameProgressScores.ToList()[2].Should().Be("90");
            result.FrameProgressScores.ToList()[1].Should().Be("60");
            result.FrameProgressScores.ToList()[0].Should().Be("30");

        }

        [Fact]
        public void CalculateAll1Throws()
        {
            int[] pinsDowned = new int[12];
            for (int i = 0; i < pinsDowned.Length; i++)
            {
                pinsDowned[i] = 1;
            }
            var result = _scoreCalculator.GetScore(pinsDowned);
            result.GameCompleted.Should().BeFalse();
            result.FrameProgressScores.ToList()[5].Should().Be("12");
            result.FrameProgressScores.ToList()[4].Should().Be("10");
            result.FrameProgressScores.ToList()[3].Should().Be("8");
            result.FrameProgressScores.ToList()[2].Should().Be("6");
            result.FrameProgressScores.ToList()[1].Should().Be("4");
            result.FrameProgressScores.ToList()[0].Should().Be("2");
        }

        [Fact]
        public void Calculate7FramesSpareAndStrikes()
        {
            int[] pinsDowned = new int[] { 1, 1, 1, 1, 9, 1, 2, 8, 9, 1, 10, 10 };
            var result = _scoreCalculator.GetScore(pinsDowned);
            result.GameCompleted.Should().BeFalse();
            result.FrameProgressScores.ToList()[6].Should().Be("*");
            result.FrameProgressScores.ToList()[5].Should().Be("*");
            result.FrameProgressScores.ToList()[4].Should().Be("55");
            result.FrameProgressScores.ToList()[3].Should().Be("35");
            result.FrameProgressScores.ToList()[2].Should().Be("16");
            result.FrameProgressScores.ToList()[1].Should().Be("4");
            result.FrameProgressScores.ToList()[0].Should().Be("2");
        }

        [Fact]
        public void CalculateAllStrikesWithNoBonusBallThrown()
        {
            int[] pinsDowned = new int[] { 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10 };
            var result = _scoreCalculator.GetScore(pinsDowned);
            result.GameCompleted.Should().BeFalse();
            result.FrameProgressScores.ToList()[9].Should().Be("*");
            result.FrameProgressScores.ToList()[8].Should().Be("270");
            result.FrameProgressScores.ToList()[7].Should().Be("240");

            result.FrameProgressScores.ToList()[6].Should().Be("210");
            result.FrameProgressScores.ToList()[5].Should().Be("180");
            result.FrameProgressScores.ToList()[4].Should().Be("150");
            result.FrameProgressScores.ToList()[3].Should().Be("120");
            result.FrameProgressScores.ToList()[2].Should().Be("90");
            result.FrameProgressScores.ToList()[1].Should().Be("60");
            result.FrameProgressScores.ToList()[0].Should().Be("30");
        }
        [Fact]
        public void CalculateIncompleteFrame1PriorStrike()
        {
            int[] pinsDowned = new int[] { 10, 10, 10, 9, 1, 5, 4, 9, 1, 10, 9 };
            var result = _scoreCalculator.GetScore(pinsDowned);
            result.GameCompleted.Should().BeFalse();

            result.FrameProgressScores.ToList()[7].Should().Be("*");

            result.FrameProgressScores.ToList()[6].Should().Be("*");
            result.FrameProgressScores.ToList()[5].Should().Be("123");
            result.FrameProgressScores.ToList()[4].Should().Be("103");
            result.FrameProgressScores.ToList()[3].Should().Be("94");
            result.FrameProgressScores.ToList()[2].Should().Be("79");
            result.FrameProgressScores.ToList()[1].Should().Be("59");
            result.FrameProgressScores.ToList()[0].Should().Be("30");
        }


        [Fact]
        public void CalculateIncompleteFrame2PriorStrike()
        {
            int[] pinsDowned = new int[] { 9, 1, 10, 10, 10, 9 };
            var result = _scoreCalculator.GetScore(pinsDowned);
            result.GameCompleted.Should().BeFalse();

            result.FrameProgressScores.ToList()[4].Should().Be("*");
            result.FrameProgressScores.ToList()[3].Should().Be("*");
            result.FrameProgressScores.ToList()[2].Should().Be("79");
            result.FrameProgressScores.ToList()[1].Should().Be("50");
            result.FrameProgressScores.ToList()[0].Should().Be("20");
        }

        [Fact]
        public void CalculateIncompleteFramePriorSpare()
        {
            int[] pinsDowned = new int[] { 10, 10, 10, 9, 1, 5, 4, 9, 1, 9 };
            var result = _scoreCalculator.GetScore(pinsDowned);
            result.GameCompleted.Should().BeFalse();
            result.FrameProgressScores.ToList()[6].Should().Be("*");
            result.FrameProgressScores.ToList()[5].Should().Be("122");
            result.FrameProgressScores.ToList()[4].Should().Be("103");
            result.FrameProgressScores.ToList()[3].Should().Be("94");
            result.FrameProgressScores.ToList()[2].Should().Be("79");
            result.FrameProgressScores.ToList()[1].Should().Be("59");
            result.FrameProgressScores.ToList()[0].Should().Be("30");
        }

        [Fact]
        public void CalculateSingleNonStrikeBowl()
        {
            int[] pinsDowned = new int[] { 5 };
            var result = _scoreCalculator.GetScore(pinsDowned);
            result.GameCompleted.Should().BeFalse();
            result.FrameProgressScores.ToList()[0].Should().Be("*");
        }

        [Fact]
        public void CalculateScoresInaFrameGreaterThan10()
        {
            int[] pinsDowned = new int[] { 8,9 };
            Action act = () => _scoreCalculator.GetScore(pinsDowned);
            act.Should().Throw<InvalidDownedPinException>();
                
        }

        [Fact]
        public void CalculateTooManyPinsDowned()
        {
            int[] pinsDowned = new int[] { 10, 10, 10, 10, 10, 10, 10, 10, 10, 1, 1, 1, 1, 1, 1, 1 };
            Action act = () => _scoreCalculator.GetScore(pinsDowned);
            act.Should().Throw<InvalidDownedPinException>();

            pinsDowned = new int[] {10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10 };
             act = () => _scoreCalculator.GetScore(pinsDowned);
            act.Should().Throw<InvalidDownedPinException>();
        }
    }
}
