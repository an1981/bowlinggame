﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BowlingScoreCalculator.API.Requests
{
    public class PinsDownedRequest
    {
        public int[] pinsDowned { get; set; }
    }
}
