﻿using BowlingScoreCalculator.API.Requests;
using BowlingScoreCalculator.BusinessLogic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BowlingScoreCalculator.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ScoreController : ControllerBase
    {
        private readonly IScoreCalculator _scoreCalculator;
        public ScoreController(IScoreCalculator scoreCalculator)
        {
            _scoreCalculator = scoreCalculator;
        }

        [HttpPost]
        public IActionResult CalculateScores([FromBody, Required] PinsDownedRequest request)
        {
            var data = _scoreCalculator.GetScore(request.pinsDowned);
            return Ok(data);

        }
    }
}
