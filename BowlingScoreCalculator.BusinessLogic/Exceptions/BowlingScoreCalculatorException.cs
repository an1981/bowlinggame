﻿using System;
 

namespace BowlingScoreCalculator.BusinessLogic.Exceptions
{
    public class BowlingScoreCalculatorException: Exception
    {
        public BowlingScoreCalculatorException(string message)
            : base(message)
        {

        }
        public string Code { get; protected set; }
        
    }
}
