﻿using BowlingScoreCalculator.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BowlingScoreCalculator.BusinessLogic
{
    public interface IScoreCalculator
    {
        BowlingResult GetScore(int[] pinsDowned);
    }
}
