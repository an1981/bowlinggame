﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BowlingScoreCalculator.BusinessLogic.Exceptions
{
    public class Error
    {
        public string Code { get; set; }
        public string Message { get; set; }
    }
}
